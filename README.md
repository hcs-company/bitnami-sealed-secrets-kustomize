Bitnami Sealed Secrets Deployment with Kustomize
================================================

This repository contains a Kustomization for deploying the Bitnami Sealed
Secrets Operator onto an OpenShift cluster.

It is based on the `controller.yaml` from the latest
[release](https://github.com/bitnami-labs/sealed-secrets/releases) (0.17.3) at
the time of writing, with a small difference; Instead of deploying into the
`kube-system` namespace this deploys into the `openshift-sealed-secrets`
namespace. This does mean that this namespace will need to be passed to the
`kubeseal` tool when fetching a certificate or encrypting secrets.
